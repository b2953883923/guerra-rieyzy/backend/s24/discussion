// console.log("Hello World!");


// [SECTION] While Loop

	/*
		- A while loop takes an expression/condition.
		- Expression/s are any unit of code that can be evaluated.
			- If expression is true, the code block is executed.
		- Iteration - term given to the repition of statements.

		Syntax:
			while(expression/condition) {
				statement/code block
			}

	*/


let count = 5;

while(count !== 0) {
	console.log("While: " + count);

	// count++; - this will cause an infite loop.
	count--;
};


// [SECTION] Do While Loop

	/*
		- A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.
	
		Syntax:
			do {
				statement
			} while(expression/condition)

	*/

let number = Number(prompt("Give me a number."));

do {
	console.log("Do While: " + number);

	// number++ (adds one after the condition has been met.)
	number += 1;
} while(number < 10)


// [SECTION] For Loop
	/*
		- A for loop is more flexible than while loop and do-while loop.

		It consists of three parts:
			1. Initialization - value that will track the progression of the loop.
			2. Expression/Condition - the condition to be evaluated. This determines whethre the loop will run one more time.
			3. Final Expression - indicates how to advance the loop (increment or decrement.)

		Syntax:
			for(initialization; expression; finalExpression) {
				statement;
			}

	*/
	

// Increasing
// for(let count = 0; count <= 20; count++) {
// 	console.log(count);
// };

// Increment by 2s
// for(let count = 0; count <= 20; count+=2) {
// 	console.log(count);
// };

// Decreasing
// for(let count = 50; count >= 20; count--) {
// 	console.log(count);
// };



// Looping with Strings

let myString = "alex";

// Characters in strings may be counted using the .length property.
// Strings are special compared to other data types in a way that it has access to functions and other pieces of information compared to the primitive data types (char)
console.log(myString.length);

// Accesssing elements for a string
// Individual characters of a string may be accessed using its index number.
console.log(myString[0]); // this is "a" from alex string
console.log(myString[1]); // this is "l" from alex string
console.log(myString[2]); // this is "e" from alex string
console.log(myString[3]); // this is "x" from alex string

//this loop will print out the individual letters of the myString variable.
//condition -> 0 < 4
for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

let myName = "AlExander";

for(let i = 0; i < myName.length; i++){
	if(myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "o" || 
		myName[i].toLowerCase() == "u"){
		// Will Print "Vowel" if we encounter a vowel letter from the string.
		console.log("Vowel")
	} else {
		// Will print the non-vowel/consonant letters
		console.log(myName[i])
	}
}

// [SECTION] Continue and break statements

	// The "continue" statement allows the code to go the next iteration of the loop without finishing the execution of all the statements in a code block.

	// The "break" statement is used to terminate the current loop once a match has been found. 
	

	for(let count = 0; count <= 20; count++){
		if(count % 2 === 0){
			// Tells the code to continue to the next iteration
			// This ignores all the statements located after the continue statement.
			continue;
			console.log("Hi")
		}
		// console.log("Continue and Break:" + count);
		// The numbers after 11 will no longer be printed.
		if(count > 10) {

			break;
		}
		// The numbers after 10 will no longer be printed.
		console.log("Continue and Break:" + count);
	}

	// Another example

	let name = "George"

	for(let i = 0; i < name.length; i++){
		console.log(name[i]);
		if(name[i].toLowerCase() === "e"){
			console.log("Continue to the next iteration");
			continue;
		}
		if(name[i].toLowerCase() === "r"){
			
			break;
		}
	}

